@echo off
goto check_Permissions

:check_Permissions
    echo Admin rights are needed. Checking rights...

cd /d "%~dp0" && ( if exist "%temp%\getadmin.vbs" del "%temp%\getadmin.vbs" ) && fsutil dirty query %systemdrive% 1>nul 2>nul || (  echo Set UAC = CreateObject^("Shell.Application"^) : UAC.ShellExecute "cmd.exe", "/k cd ""%~sdp0"" && %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs" && "%temp%\getadmin.vbs" && exit /B )
    if %errorLevel% == 0 (
        echo Success: Admin rights detected.
    ) else (
        echo ERROR! You should run this script as Admin.
		pause > nul
		exit
    )
	cls
echo Checking your OS...
for /f "tokens=4-5 delims=. " %%i in ('ver') do set version=%%i.%%j
if "%version%"=="6.3" set allow=1
if "%version%"=="6.1" set allow=1
if "%version%"=="10.0" set allow=1
if %allow%==1 goto menu
set allow=0
echo.
echo You did not pass the test.
echo This script can only be ran on Windows 7/8./10 systems.
echo If you have Windows 8, please install Windows 8.1 update from Store.
echo.
echo Press any key to exit.
pause > NUL
exit
:menu
cls
echo THIS IS AN ALPHA VERSION!! SCRIPT MAY CONTAIN TYPOS AND OTHER ERRORS!!
echo This tool has been writen for an manual Office removal if the automatic tool fails or takes ages to complete.
echo This tool supports Office 2007 and up.
echo Licensed under Creative Commons Attribution-NoDerivs license. 

color 0b
echo.
echo.
echo Menu
echo.
echo.
echo 1^) Remove Office 2007
echo 2^) Remove Office 2010
echo 3^) Remove Office 2013/365
echo 4^) Remove Office 2016/365
echo 5^) Exit
REM for any other (invalid) input:
echo.
set /p mmchoice=Choose Office version: 
if %mmchoice%==1 goto remove07
if %mmchoice%==2 goto remove10
if %mmchoice%==3 goto remove13
if %mmchoice%==4 goto remove16
if %mmchoice%==5 exit
goto error
:error
color 0C
cls
echo Selection Error!
echo.
echo.
echo You did not select an valid Office product.
echo.
echo Press any key to go back to menu.
pause > NUL
goto menu
:remove07
cls
echo Stopping Office background services and other programs
taskkill /f /im GrooveMonitor.exe
taskkill /f /im explorer.exe
taskkill /f /im skype.exe
net stop ose
net stop odserv
net stop "Microsoft Office Groove Audit Service"
echo Stopped
echo.
echo.
echo Removing folders
cd "C:\Program Files\Common Files\microsoft shared\"
rmdir /s /q "OFFICE11"
rmdir /s /q "OFFICE12"
cd C:\Program Files\
rmdir /s /q "Microsoft Works"
rmdir /s /q "Microsoft Office"
cd C:\Program Files (x86)\
rmdir /s /q "Microsoft Works"
rmdir /s /q "Microsoft Office"
cd "C:\ProgramData\Microsoft\Windows\Start Menu\Programs"
rmdir /s /q "Microsoft Office"
echo Folders removed
echo.
echo.
echo Removing registery values
reg delete "HKLM\Software\Microsoft\Office" /f
reg delete "HKCU\Software\Microsoft\Office" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{60EC980A-BDA2-4CB6-A427-B07A5498B4CA}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-0015-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-0016-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-0018-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-0019-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-001A-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-001B-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-001F-0407-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-001F-0409-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-001F-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-001F-041D-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-002C-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-0044-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-006E-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-00A1-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90120000-00BA-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{91120000-002E-0000-0000-0000000FF1CE}" /f
echo Registery values removed
echo.
echo Restarting Explorer.exe
start C:\Windows\explorer.exe
echo.
echo Done
echo.
echo Please reboot the computer (Recomended).
echo If you got an error while running this tool, please reboot the computer and try again. 
echo Press any key to go back to menu.
pause > NUL
goto menu
:remove10
cls
echo Stopping Office services and background apps.
net stop ose
net stop osppsvc
taskkill /f /im explorer.exe
taskkill /f /im skype.exe
echo.
echo Stopped
echo.
echo Folders removed
cd "C:\Program Files\Common Files\microsoft shared\"
rmdir /s /q "OFFICE14"
rmdir /s /q "OfficeSoftwareProtectionPlatform"
cd C:\Program Files\
rmdir /s /q "Microsoft Office"
cd C:\Program Files (x86)\
rmdir /s /q "Microsoft Office"
cd "C:\ProgramData\Microsoft\Windows\Start Menu\Programs"
rmdir /s /q "Microsoft Office"
rmdir /s /q "SharePoint"
echo.
echo Folder removal done.
echo.
echo.
echo Removing registery values
reg delete "HKLM\Software\Microsoft\Office" /f
reg delete "HKCU\Software\Microsoft\Office" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0015-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0015-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0016-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0016-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0018-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0018-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0019-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0019-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001A-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001A-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001B-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001B-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-0407-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-0407-0000-0000000FF1CE}_Office14.PROPLUSR_{65A2328E-FDFB-4CA3-8582-357EA6825FEA}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-0409-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-0409-0000-0000000FF1CE}_Office14.PROPLUSR_{99ACCA38-6DD3-48A8-96AE-A283C9759279}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{0EF937D0-95B1-42E3-9643-9D49E4323DF9}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-0419-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-0419-0000-0000000FF1CE}_Office14.PROPLUSR_{DD6E7CDF-BDFF-43CF-8CCE-84FBEC5ABB77}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-041D-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-001F-041D-0000-0000000FF1CE}_Office14.PROPLUSR_{D00E944F-5ECB-42FF-B58E-8FDCF2219DE8}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-002C-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-002C-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{607F3F36-0E5F-4E06-B80A-38E7E1D8BE27}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0044-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-0044-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-006E-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-006E-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{65A88F1F-5D91-4A72-BE78-A0B0B6BD45A8}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-00A1-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-00A1-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-00BA-040B-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90140000-00BA-040B-0000-0000000FF1CE}_Office14.PROPLUSR_{FD8C09D5-7564-402B-8A7D-5DB01A6AB911}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{91140000-0011-0000-0000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{91140000-0011-0000-0000-0000000FF1CE}_Office14.PROPLUSR_{047B0968-E622-4FAA-9B4B-121FA109EDDE}" /f
echo Registery values removed
echo.
echo Restarting Explorer.exe
start C:\Windows\explorer.exe
echo.
echo Done
echo.
echo Please reboot the computer (Recomended).
echo If you got an error while running this tool, please reboot the computer and try again. 
echo Press any key to go back to menu.
pause > NUL
goto menu
:remove13
cls
echo Stopping Office services and background apps.
net stop ose
net stop ClickToRunSvc
taskkill /f /im explorer.exe
taskkill /f /im skype.exe
echo.
echo Stopped
echo.
echo Removing folders
cd "C:\Program Files\Common Files\microsoft shared\"
rmdir /s /q "OFFICE15"
cd C:\Program Files\
rmdir /s /q "Microsoft Office"
cd C:\Program Files (x86)\
rmdir /s /q "Microsoft Office"
cd "C:\ProgramData\Microsoft"
rmdir /s /q "OFFICE"
cd "C:\ProgramData\Microsoft\Windows\Start Menu\Programs"
rmdir /s /q "Microsoft Office 2013"
rmdir /s /q "Microsoft Office"
echo.
echo Done
echo.
echo.
echo Removing registery values
reg delete "HKLM\Software\Microsoft\Office" /f
reg delete "HKCU\Software\Microsoft\Office" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90150000-002A-0000-1000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90150000-002A-040B-1000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\AppVISV" /f
echo Done
echo.
echo Restarting Explorer.exe
start C:\Windows\explorer.exe
echo.
echo Done
echo.
echo Please reboot the computer (Recomended).
echo One registery value might say ACCESS DENIED but that is normal. Removal has been done.
echo If you got an error while running this tool, please reboot the computer and try again. 
echo Press any key to go back to menu.
pause > NUL
goto menu
:remove16
cls
echo Stopping Office services and background apps.
net stop ose
net stop ClickToRunSvc
taskkill /f /im explorer.exe
taskkill /f /im msoia.exe
taskkill /f /im skype.exe
sc delete ClickToRunSvc
echo.
echo Stopped
echo.
echo Removing folders
cd "C:\Program Files\Common Files\microsoft shared\"
rmdir /s /q "ClickToRun"
cd C:\Program Files\
rmdir /s /q "Microsoft Office"
rmdir /s /q "Microsoft Office 15"
cd C:\Program Files (x86)\
rmdir /s /q "Microsoft Office"
rmdir /s /q "Microsoft Office 15"
cd "C:\ProgramData\Microsoft"
rmdir /s /q "Office"
rmdir /s /q "ClickToRun"
cd "C:\ProgramData\Microsoft\Windows\Start Menu\Programs"
rmdir /s /q "Microsoft Office 2016 Tools"
del "Access 2016.ink"
del "Excel 2016.ink"
del "Word 2016.ink"
del "PowerPoint 2016.ink"
del "Outlook 2016.ink"
del "Skype for Business 2016.ink"
del "Publisher 2016.ink"
del "OneNote 2016.ink"
echo.
echo Done
echo.
echo.
echo Removing registery values
reg delete "HKLM\Software\Microsoft\Office" /f
reg delete "HKCU\Software\Microsoft\Office" /f
reg delete "HKLM\Software\Microsoft\Windows\CurrentVersion\Uninstall\{90160000-008F-0000-1000-0000000FF1CE}" /f
reg delete "HKLM\Software\Microsoft\AppVISV" /f
echo Done
echo.
echo Restarting Explorer.exe
start C:\Windows\explorer.exe
echo.
echo Done
echo.
echo Please reboot the computer (Recomended).
echo One registery value might say ACCESS DENIED but that is normal. Removal has been done.
echo If you got an error while running this tool, please reboot the computer and try again. 
echo Press any key to go back to menu.
pause > NUL
goto menu